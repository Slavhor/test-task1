<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Employee;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create(['title' => 'Отдел закупок'])->employees()->attach(Employee::factory(3)->create());
        Department::create(['title' => 'Отдел продаж'])->employees()->attach(Employee::factory(3)->create());
        Department::create(['title' => 'PR-отдел'])->employees()->attach(Employee::factory(3)->create());
    }
}
