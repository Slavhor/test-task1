<div>
    <div class="card">
        <div class="card-header">
            @lang("Сетка")
        </div>
        <div class="card-body">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th scope="col"></th>
                    @foreach($departments as $department)
                        <th scope="col" class="text-center">{{$department->title}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                    @foreach($employees as $employee)
                        <tr>
                            <th scope="row">{{ $employee->first_name.' '.$employee->last_name }}</th>
                            @foreach($departments as $department)
                                <td class="text-center">@if($department->employees->contains($employee)) ✓ @endif</td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
