<div>
    @include('partials.alerts')

    <div class="card">
        <div class="card-header">
            @lang("Отделы")
        </div>
        <div class="card-body">
            <button wire:click="add()" type="button" class="btn btn-primary mb-3">@lang('Добавить')</button>
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th scope="col">@lang('Название отдела')</th>
                    <th scope="col">@lang('Количество сотрудников')</th>
                    <th scope="col">@lang('Максимальная заработная плата')</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($departments as $department)
                        <tr>
                            <td>{{$department->title}}</td>
                            <td>{{$department->employees_count}}</td>
                            <td>{{$department->employees_max_salary}}</td>
                            <td>
                                <button wire:click="edit({{$department->id}})" type="button" class="btn btn-primary">@lang("Edit")</button>
                                <button wire:click="delete({{$department->id}})" type="button" class="btn btn-danger">@lang("Delete")</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('livewire.departments.add-modal')

    @section('scripts')
        <script>
            window.addEventListener('closeDepartmentModal',function (){
                $('#departmentModal').modal('hide');
            });
            window.addEventListener('showDepartmentModal',function (){
                $('#departmentModal').modal('show');
            });
        </script>
    @endsection
</div>
