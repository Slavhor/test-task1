    <div wire:ignore.self id="departmentModal" class="modal fade" tabindex="-1" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        @if($update)
                            @lang('Редактировать отдел')
                        @else
                            @lang('Новый отдел')
                        @endif
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                @include('partials.input-alerts')

                <form wire:submit.prevent="{{($update) ? 'update('.$departmentId.')' : 'store()' }}">
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="departmentTitle" class="form-label">@lang('Название отдела')</label>
                            <input required wire:model.defer="department.title" type="text" class="form-control @error('department.title') is-invalid @enderror" id="departmentTitle">
                            <div class="invalid-feedback">
                                @error('department.title') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('Отмена')</button>
                        <button type="submit" class="btn btn-primary" >@lang('Сохранить')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
