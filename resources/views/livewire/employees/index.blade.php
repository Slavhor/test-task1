<div>
    @include('partials.alerts')

    <div class="card">
        <div class="card-header">
            @lang("Сотрудники")
        </div>
        <div class="card-body">
            <button wire:click="add()" type="button" class="btn btn-primary mb-3">@lang('Добавить')</button>
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th scope="col">@lang('Имя')</th>
                    <th scope="col">@lang('Фамилия')</th>
                    <th scope="col">@lang('Отчество')</th>
                    <th scope="col">@lang('Пол')</th>
                    <th scope="col">@lang('Заработная плата')</th>
                    <th scope="col">@lang('Отделы')</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <td>{{$employee->first_name}}</td>
                        <td>{{$employee->last_name}}</td>
                        <td>{{$employee->middle_name}}</td>
                        <td>{{$employee->gender}}</td>
                        <td>{{$employee->salary}}</td>
                        <td>{{$employee->departments->implode('title', ', ')}}</td>
                        <td>
                            <button wire:click="edit({{$employee->id}})" type="button" class="btn btn-primary">@lang("Edit")</button>
                            <button wire:click="delete({{$employee->id}})" type="button" class="btn btn-danger">@lang("Delete")</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('livewire.employees.add-modal')

    @section('scripts')
        <script>
            window.addEventListener('closeEmployeeModal',function (){
                $('#employeeModal').modal('hide');
            });
            window.addEventListener('showEmployeeModal',function (){
                $('#employeeModal').modal('show');
            });
        </script>
    @endsection
</div>
