<div wire:ignore.self id="employeeModal" class="modal fade" tabindex="-1" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    @if($update)
                        @lang('Редактировать сотрудника')
                    @else
                        @lang('Новый сотрудник')
                    @endif
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            @include('partials.input-alerts')

            <form wire:submit.prevent="{{($update) ? 'update('.$employeeId.')' : 'store()' }}">
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="employeeFirstName" class="form-label">@lang('Имя')</label>
                        <input required wire:model.defer="employee.first_name" type="text" class="form-control @error('employee.first_name') is-invalid @enderror" id="employeeFirstName">
                        <div class="invalid-feedback">
                            @error('employee.first_name') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="employeeLastName" class="form-label">@lang('Фамилия')</label>
                        <input required wire:model.defer="employee.last_name" type="text" class="form-control @error('employee.last_name') is-invalid @enderror" id="employeeLastName">
                        <div class="invalid-feedback">
                            @error('employee.last_name') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="employeeMiddleName" class="form-label">@lang('Отчество')</label>
                        <input wire:model.defer="employee.middle_name" type="text" class="form-control @error('employee.middle_name') is-invalid @enderror" id="employeeMiddleName">
                        <div class="invalid-feedback">
                            @error('employee.middle_name') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="employeeGender" class="form-label">@lang('Пол')</label>
                        <select wire:model.defer="employee.gender" class="custom-select @error('employee.gender') is-invalid @enderror" id="employeeGender">
                            <option selected value="">@lang('Выберите пол')</option>
                            <option value="male">@lang('male')</option>
                            <option value="female">@lang('female')</option>
                        </select>
                        <div class="invalid-feedback">
                            @error('employee.gender') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="employeeSalary" class="form-label">@lang('Заработная плата')</label>
                        <input wire:model.defer="employee.salary" type="number" step="1" min="0" class="form-control @error('employee.salary') is-invalid @enderror" id="employeeSalary">
                        <div class="invalid-feedback">
                            @error('employee.salary') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label for="employeeDepartments" class="form-label">@lang('Отделы')</label>
                        <select wire:model.defer="employee.departments" required multiple class="form-control select2 @error('employee.departments') is-invalid @enderror" id="employeeDepartments">
                            @foreach($departments as $department)
                                <option value="{{$department->id}}">{{$department->title}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            @error('employee.departments') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('Отмена')</button>
                    <button type="submit" class="btn btn-primary" >@lang('Сохранить')</button>
                </div>
            </form>
        </div>
    </div>
</div>
