<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', \App\Http\Livewire\Grid::class)->name('grid');
Route::get('/departments', \App\Http\Livewire\Departments::class)->name('departments');
Route::get('/employees', \App\Http\Livewire\Employees::class)->name('employees');

