<?php

namespace App\Http\Livewire;

use App\Models\Department;
use App\Models\Employee;
use Livewire\Component;

class Employees extends Component
{
    //List of Departments for add/edit form
    public $departments;

    //List of Employees for table
    public $employees;

    //Data for add/edit Employee
    public $employee = [];
    public $employeeId;

    //Is modal for add or update
    public $update = false;

    protected $rules = [
        'employee.first_name' => 'required',
        'employee.last_name' => 'required',
        'employee.salary' => 'integer',
        'employee.departments' => 'required|array',
    ];

    public function render()
    {
        $this->employees = Employee::with('departments')->get();
        $this->departments = Department::all();
        return view('livewire.employees.index');
    }

    //Open add new ModalForm
    public function add()
    {
        $this->update = false;
        $this->resetInputFields();

        $this->dispatchBrowserEvent('showEmployeeModal');
    }

    //Open edit ModalForm
    public function edit(Employee $employee)
    {
        $this->update = true;
        $this->employeeId = $employee->id;
        $this->employee = $employee->toArray();
        $this->employee['departments'] = $employee->departments->pluck('id')->toArray();

        $this->dispatchBrowserEvent('showEmployeeModal');
    }

    public function store()
    {
        $this->validate();

        $employee = Employee::create($this->employee);
        $employee->departments()->sync($this->employee['departments']);

        session()->flash('success',__('Успешно добавлено.'));
        $this->closeModal();
    }

    public function update(Employee $employee)
    {
        $this->validate();

        $employee->update($this->employee);
        $employee->departments()->sync($this->employee['departments']);

        session()->flash('success',__('Успешно сохранено.'));
        $this->closeModal();
    }

    public function delete(Employee $employee)
    {
        if($employee->delete()){
            session()->flash('success',__('Успешно удалено.'));
        }
    }

    private function closeModal()
    {
        $this->resetInputFields();
        $this->dispatchBrowserEvent('closeEmployeeModal');
    }

    private function resetInputFields()
    {
        $this->reset(['employee', 'employeeId']);
        $this->resetErrorBag();
    }
}
