<?php

namespace App\Http\Livewire;

use App\Models\Department;
use App\Models\Employee;
use Livewire\Component;

class Grid extends Component
{
    public $departments;
    public $employees;

    public function mount()
    {

        $this->departments = Department::with('employees')->get();
        $this->employees = Employee::all();
    }

    public function render()
    {
        return view('livewire.grid');
    }
}
