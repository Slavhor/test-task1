<?php

namespace App\Http\Livewire;

use App\Models\Department;
use Livewire\Component;

class Departments extends Component
{
    //List of Departments for table
    public $departments;

    //Data for add/edit Department
    public $department = [];
    public $departmentId;

    //Is modal for add or update
    public $update = false;
    public $title;

    protected $rules = [
        'department.title' => 'required',
    ];

    public function render()
    {
        $this->departments = Department::withCount('employees')
            ->withMax('employees','salary')
            ->get();
        return view('livewire.departments.index');
    }

    public function add()
    {
        $this->update = false;
        $this->resetInputFields();

        $this->dispatchBrowserEvent('showDepartmentModal');
    }

    public function edit(Department $department)
    {
        $this->update = true;
        $this->department = $department->toArray();
        $this->departmentId = $department->id;

        $this->dispatchBrowserEvent('showDepartmentModal');
    }

    public function store()
    {
        $this->validate();

        Department::create($this->department);

        session()->flash('success',__('Успешно добавлено.'));
        $this->closeModal();
    }

    public function update(Department $department)
    {
        $this->validate();

        $department->update($this->department);

        session()->flash('success',__('Успешно сохранено.'));
        $this->closeModal();
    }

    public function delete(Department $department)
    {
        if($department->delete()){
            session()->flash('success',__('Успешно удалено.'));
        }
    }

    private function closeModal()
    {
        $this->resetInputFields();
        $this->dispatchBrowserEvent('closeDepartmentModal');
    }

    private function resetInputFields()
    {
        $this->reset(['department', 'departmentId']);
        $this->resetErrorBag();
    }
}
