<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $fillable = ['title'];

    public function employees(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Employee::class);
    }

    public function delete()
    {
        if($this->employees->count() > 0 ){

            session()->flash('warning',__('В отделе есть сотрудники. Удаление запрещено.'));
            return false;
        };

        return parent::delete();
    }
}
